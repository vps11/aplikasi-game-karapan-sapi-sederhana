package com.coba2.simplegame01;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;

public class Home extends Game implements Screen {
    public Stage mainStage;
    public int garisFinish = 160000; // jarak lintasan
    private AnimatedActor pemainSapi, pemainSapiLawan;
    private BaseActor areaMenang, tanah, andaMenang, andaKalah;
    private Action efekWarna = Actions.forever(Actions.sequence(Actions.color(Color.BLUE, 1), Actions.color(Color.GREEN, 1)));
    private float waktu, energi;
    private Label labelUkur, ulang, edukasi;
    private int kecepatanPemainSapi;

    public void create() {
        mainStage = new Stage();
        tanah = new BaseActor();
        tanah.setTexture(new Texture(Gdx.files.internal("rumput.png"))); // garis start
        tanah.setPosition(60, 0);
        mainStage.addActor(tanah);
        areaMenang = new BaseActor();
        areaMenang.setTexture(new Texture(Gdx.files.internal("rumput.png"))); // garis finish
        areaMenang.setPosition(garisFinish, 0);
        mainStage.addActor(areaMenang);
        // pemain sapi
        TextureRegion[] gerakanSapi = new TextureRegion[3];
        for(int n = 0; n < 3; n++) {
            String namaFile = "sapi" + n + ".png";
            Texture duaSapi = new Texture(Gdx.files.internal(namaFile));
            duaSapi.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            gerakanSapi[n] = new TextureRegion(duaSapi);
        }
        Array<TextureRegion> arraySapi = new Array<TextureRegion>(gerakanSapi);
        Animation<TextureRegion> animasiSapi = new Animation<TextureRegion>(0.1f, arraySapi, Animation.PlayMode.LOOP_PINGPONG);
        pemainSapi = new AnimatedActor();
        pemainSapi.setAnimation(animasiSapi);
        pemainSapi.setPosition(10, 10);
        pemainSapi.setBounds(10, 10, pemainSapi.getWidth(), pemainSapi.getHeight());
        mainStage.addActor(pemainSapi);
        pemainSapiLawan = new AnimatedActor();
        pemainSapiLawan.setAnimation(animasiSapi);
        pemainSapiLawan.setPosition(10, 90);
        mainStage.addActor(pemainSapiLawan);
        // tulisan anda menang
        andaMenang = new BaseActor();
        andaMenang.setTexture(new Texture(Gdx.files.internal("menang.png")));
        andaMenang.setVisible(false);
        andaMenang.addAction(efekWarna);
        mainStage.addActor(andaMenang);
        // tulisan anda kalah
        andaKalah = new BaseActor();
        andaKalah.setTexture(new Texture(Gdx.files.internal("kalah.png")));
        andaKalah.setVisible(false);
        mainStage.addActor(andaKalah);
        // label
        FreeTypeFontGenerator pengolahFont = new FreeTypeFontGenerator(Gdx.files.internal("times.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 16; // ukuran font
        BitmapFont font = pengolahFont.generateFont(parameter); // konversi font ke bitmap
        pengolahFont.dispose();
        Label.LabelStyle style = new Label.LabelStyle(font, Color.BLACK);
        labelUkur = new Label("", style);
        labelUkur.setFontScale(1);
        mainStage.addActor(labelUkur);
        waktu = 0;
        ulang = new Label("[Restart]", style);
        ulang.setFontScale(1);
        ulang.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                pemainSapiLawan.velocityX = 0;
                pemainSapiLawan.setX(10);
                kecepatanPemainSapi = 0;
                pemainSapi.velocityX = 0;
                pemainSapi.setX(10);
                andaMenang.setVisible(false);
                andaKalah.setVisible(false);
                waktu = 0;
                energi = 0;
                super.clicked(event, x, y);
            }
        });
        mainStage.addActor(ulang);
        String teksEdukasi = "Karapan sapi adalah budaya khas Madura yang digelar setiap tahun pada bulan Agustus atau September, dan dilombakan lagi pada final akhir bulan September atau Oktober. Terdapat seorang joki dan 2 ekor sapi yang dipaksa untuk berlari sekencang mungkin sampai garis finish. -Vijay P.S.";
        edukasi = new Label(teksEdukasi, style);
        edukasi.setFontScale(1);
        edukasi.setWrap(true);
        edukasi.setWidth(600);
        edukasi.setPosition(120, 400, Align.topLeft);
        mainStage.addActor(edukasi);
    }

    @Override
    public void render() {
        Gdx.input.setInputProcessor(mainStage);
        float dt = Gdx.graphics.getDeltaTime();
        mainStage.act(dt);
        if(pemainSapi.getX() == 10) {
            waktu = 0;
        }
        else if(pemainSapi.getX() < garisFinish) {
            waktu += dt;
        }
        if(9 < pemainSapi.getX() && pemainSapi.getX() < garisFinish) { // 9 < x < garisFinish
            if(Gdx.input.isTouched()) {
                int aksel = 15; // 10 = sekitar 600 pixel/s2
                kecepatanPemainSapi = (int) pemainSapi.velocityX;
                kecepatanPemainSapi += aksel;
                pemainSapi.velocityX += aksel;
                pemainSapiLawan.velocityX += 5;
                energi += (84.4 + (9 * kecepatanPemainSapi)) * 4.184; // Marco dan Aello, 1998, bbrp faktor diabaikan
            }
            else {
                kecepatanPemainSapi = 0;
                if(pemainSapi.velocityX > 0) {
                    pemainSapi.velocityX -= 10;
                }
                else {
                    pemainSapi.velocityX = 0;
                }
            }
        }
        else if(pemainSapi.getX() > garisFinish) {
            pemainSapiLawan.velocityX = 0;
            kecepatanPemainSapi = 0;
            pemainSapi.velocityX = 0;
            andaKalah.setVisible(pemainSapi.getX() < pemainSapiLawan.getX());
            andaMenang.setVisible(pemainSapi.getX() >= pemainSapiLawan.getX());
        }
        Camera kamera = mainStage.getCamera();
        kamera.position.set(pemainSapi.getX(), 240, Align.center);                               // posisi kamera
        kamera.position.x = MathUtils.clamp(kamera.position.x, 400, (garisFinish + 100) - 360); // ^
        kamera.position.y = 240;                                                                   // ^
        kamera.update();
        labelUkur.setText("Jarak: " + garisFinish + ",  t: " + (int) waktu + ",  dx: " + (int) (pemainSapi.getX() - pemainSapiLawan.getX()) +
                ",  v/100: " + (int) (pemainSapi.velocityX / 100) + ",  E/1jt: " + (int) (energi / 1000000));
        labelUkur.setPosition(kamera.position.x - 300, 450, Align.bottomLeft);
        ulang.setPosition(kamera.position.x + 270, 450, Align.bottomRight);
        andaKalah.setPosition(kamera.position.x, 240, Align.center);
        andaMenang.setPosition(kamera.position.x, 240, Align.center);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mainStage.draw();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void hide() {

    }
}